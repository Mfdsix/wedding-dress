<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Wedding Dress.co</title>
	<link rel="stylesheet" type="text/css" href="./assets/css/bootstrap.min.css">
</head>
<body>

	<?php
	include('./oop/db.php');
	require('./oop/partner.php');
	require('./oop/match.php');
	session_start();

	$dt = $_SESSION['user_id'];

	$PARTNER = new Partner($conn);
	$MATCH = new Match($conn);

	if($match_id = $MATCH->checkMatch($dt)){
		echo '<script>alert("Anda Telah Menenukan Partner");window.location.href="./dress.php"</script>';
	}else{
		if($foundpartner = $PARTNER->findPartner($dt)){
			$malecode = ($foundpartner[0] == "01") ? $foundpartner[1] : $dt;
			$femalecode = ($foundpartner[0] == "01") ? $dt : $foundpartner[1];

			if($MATCH->createMatch($malecode, $femalecode)){
				echo '<script>alert("Anda Telah Menenukan Partner");window.location.href="./dress.php"</script>';
			}else{
				echo '<script>alert("Gagal Menghubungkan dengan Partner");</script>';
			}
		}
	}
	?>

<div class="container" style="min-height: 100vh; display: flex;justify-content: center;  align-items: center;">
	<div class="text-center">
		<p>
			<img src="<?= $_SESSION['photo'] ;?>" height="80px"><br>
			<b><?= $dt ;?></b>
		</p>
		<h3>Selamat Datang <b><?= $_SESSION['fullname'] ;?></b></h3>
		<p>silahkan menunggu hingga kami menemukan pasangan anda</p>
		<div>
			<a class="btn btn-primary" href="./home.php">Refresh</a>
			<a class="btn btn-danger" href="./logout.php">Logout</a>
		</div>
	</div>
</div>

</body>
</html>