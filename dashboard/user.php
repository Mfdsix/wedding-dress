<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Dashboard - Wedding Dress.co</title>
	<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
</head>
<body>

<?php 

include('../oop/db.php');
require('../oop/user.php');

$USER = new User($conn);

session_start();

$users = $USER->getUsers();
 ?>

<div class="container" style="min-height: 100vh; display: flex;justify-content: center;  align-items: center;">
	<div>
	<div class="row w-100">
		<div class="col-md-4 text-center bg-primary text-white">
			<h3>WD.co</h3>
		</div>
		<div class="col-md-8 text-end">
			<a href="/dashboard">Home</a>
			<a href="/dashboard/user.php">User</a>
		</div>
	</div>

	<hr>
	

	<div>
		<h4>Use yang terdaftar</h4>

		<?php 
		foreach($users as $k => $value){
			?>
			<div class="card">
				<div class="card-body">
				<div class="row">
					<div class="col-md-3">
						<img src="<?= '../'.$value['photo'] ;?>" height="80px">
					</div>
					<div class="col-md-6">
						<p><?= $value['fullname'] . ' ('.$value['username'].')' ;?></p>
						<span class="badge bg-primary"><?=$value['status'];?></span>
					</div>
					<div class="col-md-3">
						<a href="/dashboard/edit-user.php?id=<?=$value['id'];?>">edit</a>
						<a href="/dashboard/ban-user.php?id=<?=$value['id'];?>">ban</a>
					</div>
				</div>
					
				</div>
			</div>
			<?php 

		}
		?>
	</div>
	</div>
</div>

</body>
</html>
