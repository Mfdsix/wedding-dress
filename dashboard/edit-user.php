<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Dashboard - Wedding Dress.co</title>
	<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
</head>
<body>

<?php 

include('../oop/db.php');
require('../oop/user.php');

$USER = new User($conn);

session_start();
$USERID = $_GET['id'];

if($userdata = $USER->getUser($USERID)){
	if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST)){
		$fullname = $_POST['fullname'];

		if($USER->updateUser($USERID, [
			'fullname' => $fullname
		])){
			echo '<script>alert("Berhasil Mengupdate User");</script>';		
		}else{
			echo '<script>alert("Gagal Mengupdate User");</script>';		
		}
	}
}else{
	echo '<script>alert("User Tidak Ditemukan");window.location.href="/dashboard/user.php"</script>';
}
 ?>

<div class="container" style="min-height: 100vh; display: flex;justify-content: center;  align-items: center;">
	<div>
	<div class="row w-100">
		<div class="col-md-4 text-center bg-primary text-white">
			<h3>WD.co</h3>
		</div>
		<div class="col-md-8 text-end">
			<a href="/dashboard">Home</a>
			<a href="/dashboard/user.php">User</a>
		</div>
	</div>

	<hr>
	

	<div>
		<h4>Edit User <b><?= $userdata['fullname'] ;?></b></h4>

		<form method="POST">
			<div class="mb-3">
				<label>Dating Code</label>
				<input class="form-control" type="text" disabled value="<?= $userdata['username'] ;?>">
			</div>
			<div class="mb-3">
				<label>Nama Lengkap</label>
				<input class="form-control" type="text" name="fullname" value="<?= $userdata['fullname'] ;?>">
			</div>
			<div class="mt-1 text-end">
				<button class="btn btn-primary">Simpan</button>
			</div>
		</form>
	</div>
	</div>
</div>

</body>
</html>
