<?php
class Transaction{
	private $conn;
	function __construct($conn){
		$this->conn = $conn;
	}

	public function checkTransaction($match_id){
		$sql = "SELECT id, status, total_price FROM transactions WHERE match_id = '".$match_id."'";
$result = $this->conn->query($sql);

if ($result->rowCount() > 0) {
  return $result->fetch();
} else {
  return false;
}
	}

	public function getTransactionItems($transaction_id){
		$sql = "SELECT male_dress_id, female_dress_id FROM transactions WHERE id = '".$transaction_id."'";
$result = $this->conn->query($sql);

if($result->rowCount() > 0){
	$trx = $result->fetch();
	$sql = "SELECT title, photo FROM dresses WHERE id IN ('".$trx['male_dress_id']."', '".$trx['female_dress_id']."')";
$result = $this->conn->query($sql);

if ($result->rowCount() > 0) {
  return $result->fetchAll();
} else {
  return true;
}		
}else{
	return [];
}
	}

	public function createTransaction($data){
		$sql = "INSERT INTO transactions (match_id, male_dress_id, female_dress_id, total_price, status) VALUES ('".$data['match_id']."', '".$data['male_dress_id']."', '".$data['female_dress_id']."', ".$data["total_price"].", 'pending')";

if ($this->conn->query($sql)) {
	return true;
}else{
	return false;
	}
}

public function payTransaction($transaction_id){
$sql = "UPDATE transactions SET status = 'paid' WHERE id = '".$transaction_id."'";

if ($this->conn->query($sql)) {
	return true;
}else{
	return false;
	}	
}

}