 <?php
$servername = "localhost";
$username = "root";
$password = "pwd123";
$dbName = "wedding-dress";

try {
  $conn = new PDO("mysql:host=$servername;dbname=".$dbName, $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
  throw new Exception("Error Connection to Database", 1);
  
}
?> 
