<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Checkout - Wedding Dress.co</title>
	<link rel="stylesheet" type="text/css" href="./assets/css/bootstrap.min.css">
</head>
<body>

<?php
	include('./oop/db.php');
	require('./oop/transaction.php');
	require('./oop/match.php');
	session_start();

	$TRANSACTION = new Transaction($conn);
	$MATCH = new Match($conn);

	$dt = $_SESSION['user_id'];

	if($matchadata = $MATCH->checkMatch($dt)){
		if($trxdata = $TRANSACTION->checkTransaction($matchadata['id'])){
			$trxitems = $TRANSACTION->getTransactionItems($trxdata['id']);

			if($_SERVER["REQUEST_METHOD"] == "POST"){
				if($TRANSACTION->payTransaction($trxdata['id'])){
			echo '<script>alert("Pembayaran Berhasil");window.location.href="./checkout.php"</script>';					
				}else{
			echo '<script>alert("Gagal Melakukan Pembayaran");</script>';			
				}
			}
		}else{
			echo '<script>alert("Anda Belum Memilih Pakaian");window.location.href="./dress.php"</script>';	
		}
	}else{
		echo '<script>alert("Anda Belum Menenukan Partner");window.location.href="./home.php"</script>';
	}

	?>


<div class="container" style="min-height: 100vh; display: flex;justify-content: center;  align-items: center;">
	<div>
	<div class="text-center">
		<h3>Checkout</h3>
		<p>silahkan lakukan pembayaran untuk pakaian anda</p>
	</div>
		<div>
			<p>Pakaian yang dipilih</p>
			<div class="text-center">
				<?php 
				foreach($trxitems as $key => $value){
					?>
					<div class="card">
						<div class="card-body">
									<img src="<?=$value['photo'];?>" height="100px">
									<h4><?= $value['title'] ;?></h4>
						</div>
					</div>
<?php 
				}
				 ?>
			<hr>
			</div>
		</div>
			<div>
				<b>Total Harga</b>
				<h4>Rp <?= $trxdata['total_price'] ;?></h4>
			</div>
			<hr>
			<?php 
			if($trxdata['status'] == 'pending'){
				?>
				<form method="POST" class="text-end">
				<button class="btn btn-primary">Bayar Sekarang</button>
			</form>
				<?php 
			}else{
?>
<div class="">
	<h4 class="text-success">LUNAS</h4>
</div>
<?php 
			}
			 ?>
	</div>
</div>

</body>
</html>