<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Login - Wedding Dress.co</title>
	<link rel="stylesheet" type="text/css" href="./assets/css/bootstrap.min.css">
</head>
<body>


	<?php
	include('./oop/db.php');
	require('./oop/user.php');
	require('./oop/partner.php');

	$USER = new User($conn);
	$PARTNER = new Partner($conn);

	session_start();

	if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST)){
		$username = $_POST['username'];
		$password = $_POST['password'];

		if($userlogin = $USER->login($username, $password)){

			$USER->checkBan($userlogin['username']);

			$_SESSION["user_id"] = $userlogin["username"];
$_SESSION["fullname"] = $userlogin["fullname"];
$_SESSION["photo"] = $userlogin["photo"];
$_SESSION["role"] = $userlogin["role"];

if($userlogin['role'] == 'user'){
	echo "<script>alert('Login Berhasil');window.location.href='./home.php'</script>";
}else{
			echo "<script>alert('Login Berhasil');window.location.href='./dashboard'</script>";
}
		}else{
			echo "<script>alert('Usename atau Password Salah')</script>";
		}
	}
	?>

	<div class="container py-5" style="min-height: 100vh; display: flex;justify-content: center;  align-items: center;">
		<div class="text-center">
			<h3>Login</h3>
			<p>silahkan mengisi form dibawah untuk login</p>

			<form method="POST" class="mt-4 text-start" enctype="multipart/form-data">
				<div class="mb-3">
					<label>Email / Dating Code</label>
					<input type="text" name="username" class="form-control" required>
				</div>	

				<div class="mb-3">
					<label>Password</label>
					<input type="password" name="password" class="form-control" required>
				</div>
				<div class="mt-1 text-end">
				<button class="btn btn-primary">Login</button>
			</div>
			</form>
		</div>
	</div>