<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Daftar - Wedding Dress.co</title>
	<link rel="stylesheet" type="text/css" href="./assets/css/bootstrap.min.css">
</head>
<body>


	<?php
	include('./oop/db.php');
	require('./oop/user.php');
	require('./oop/partner.php');

	$USER = new User($conn);
	$PARTNER = new Partner($conn);

	session_start();

	if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST)){
		$fullname = $_POST['fullname'];
		$dt = $_POST['dt'];
		$dob = $_POST['dob'];
		$gender = $_POST['gender'];
		$email = $_POST['email'];
		$phone = $_POST['phone'];
		$password = md5($_POST['password']);

		$finalDt = 'SKY' . substr($dt, strlen($dt)-3, strlen($dt)) . ($gender == 'M' ? '01' : '02');

		if($PARTNER->checkDT($finalDt)){
		if(isset($_FILES['photo'])){
		$file = $_FILES['photo'];
		$target_dir = "./uploads/";
		$target_file = $target_dir . uniqid() .'-'. basename($file["name"]);
		$uploadOk = 1;
		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		$check = getimagesize($file["tmp_name"]);
		if($check !== false) {
			if (move_uploaded_file($file["tmp_name"], $target_file)) {
				
				$register = $USER->register([
					'fullname' => $fullname,
					'dt' => $finalDt,
					'dob' => $dob,
					'gender' => $gender,
					'email' => $email,
					'phone' => $phone,
					'photo' => $target_file,
					'password' => $password
				]);

				if($register){
					echo "<script>alert('Selamat Akun Anda Berhasil Dibuat, Anda Dapat Login Menggunakan ".$email." atau ".$finalDt."');window.location.href='./login.php'</script>";	
				}else{
					echo "<script>alert('Pendaftaran Gagal')</script>";	
				}

			} else {
				echo "<script>alert('Upload Gamber Bermasalah')</script>";
			}
		} else {
			echo "<script>alert('File Harus Berbentuk Gambar')</script>";
		}	
		}
	}else{
		echo "<script>alert('ID DT Sudah Terpakai')</script>";
	}
}
	?>

<div class="container py-5" style="min-height: 100vh; display: flex;justify-content: center;  align-items: center;">
	<div class="text-center">
		<h3>Daftar</h3>
		<p>silahkan mengisi form dibawah untuk mendaftar</p>

		<form method="POST" class="mt-4 text-start" enctype="multipart/form-data">
			<div class="mb-3">
				<label>Nama Lengkap</label>
				<input type="text" name="fullname" class="form-control" required>
			</div>	

			<div class="mb-3">
				<label>Dating Code</label>
				<input type="text" name="dt" class="form-control" required>
			</div>
			<div class="mb-3">
				<label>Tanggal Lahir</label>
				<input type="date" name="dob" class="form-control" required>
			</div>
			<div class="mb-3">
				<label>Gender</label>
				<p class="mb-0">
				<input type="radio" name="gender" required value="M" checked>
					Pria
				</p>
				<p class="mb-0">
				<input type="radio" name="gender" required value="F">
					Wanita
				</p>
			</div>
			<div class="mb-3">
				<label>Email</label>
				<input type="email" name="email" class="form-control" required>
			</div>	
			<div class="mb-3">
				<label>Phone</label>
				<input type="text" name="phone" class="form-control" required>
			</div>	
			<div class="mb-3">
				<label>Foto</label>
				<input type="file" name="photo" class="form-control" required>
			</div>
			<div class="mb-3">
				<label>Password</label>
				<input type="password" name="password" class="form-control" required>
			</div>	
			<div class="mt-1 text-end">
				<button class="btn btn-primary">Daftar</button>
			</div>
		</form>
	</div>
</div>

</body>
</html>