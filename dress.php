<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Pilih Baju - Wedding Dress.co</title>
	<link rel="stylesheet" type="text/css" href="./assets/css/bootstrap.min.css">
</head>
<body>

	<?php
	include('./oop/db.php');
	require('./oop/partner.php');
	require('./oop/match.php');
	require('./oop/dress.php');
	require('./oop/transaction.php');
	session_start();

	$dt = $_SESSION['user_id'];

	$PARTNER = new Partner($conn);
	$MATCH = new Match($conn);
	$DRESS = new Dress($conn);
	$TRANSACTION = new Transaction($conn);

	if($matchdata = $MATCH->checkMatch($dt)){
		if($trxdata = $TRANSACTION->checkTransaction($matchdata['id'])){
			echo '<script>alert("Anda Sudah Memilih Pakaian");window.location.href="./checkout.php"</script>';	
			return;
		}
		if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST)){
		$maledress = explode('|', $_POST['male_dress_id']);
		$femaledress = explode('|', $_POST['female_dress_id']);

		if($TRANSACTION->createTransaction([
			'male_dress_id' => $maledress[0],
			'female_dress_id' => $femaledress[0],
			'total_price' => $maledress[1] + $femaledress[1],
			'match_id' => $matchdata['id']
		])){
			echo '<script>alert("Pakaian Telah Dipilih");window.location.href="./checkout.php"</script>';
		}else{
			echo '<script>alert("Gagal Memilih Pakaian");"</script>';
		}
	}

		$partnercode = ($matchdata['male_code'] == $dt) ? $matchdata['female_code'] : $matchdata['male_code'];
		$partnerdata = $PARTNER->getPartner($partnercode);
	}else{
		echo '<script>alert("Anda Belum Menenukan Partner");window.location.href="./home.php"</script>';
	}	
	?>


	<div class="container" style="min-height: 100vh; display: flex;justify-content: center;  align-items: center;">
	<div class="text-center">
		<h3>Here is your wedding partner</h3>
		<div class="row">
			<div class="col-2"></div>
			<div class="col-4">
		<p>
			<img src="<?= $_SESSION['photo'] ;?>" height="80px"><br>
			<b><?= $_SESSION['fullname'] ;?></b>
		</p>
			</div>
			<div class="col-4">
				<p>
			<img src="<?= $partnerdata['photo'] ;?>" height="80px"><br>
			<b><?= $partnerdata['name'] ;?></b>
		</p>
			</div>
			<div class="col-2"></div>
		</div>
		<hr>
		<p>silahkan memilih pakaian</p>
		<form method="POST">
			<?php
			$maleDress = $DRESS->getDress("M");
			?>
			<div class="row">
			<?php
			foreach ($maleDress as $key => $value){
			?>	
			
			<div class="col-4">
				<p>
				<img src="<?=$value['photo'];?>" height="100px">
				</p>
				<p><?= $value["title"] ;?></p>
				<b>Rp <?= $value["price"] ;?></b>
				<hr>
				<p>
					<input type="radio" name="male_dress_id" value="<?=$value["id"].'|'.$value['price'];?>" required> Pilih
				</p>
			</div>

			<?php
		}
			?>
</div>
			<hr>

			<?php
			$femaleDress = $DRESS->getDress("F");
			?>
			<div class="row">
			<?php
			foreach ($femaleDress as $key => $value){
			?>	
			
			<div class="col-4">
				<p>
				<img src="<?=$value['photo'];?>" height="100px">
				</p>
				<p><?= $value["title"] ;?></p>
				<b>Rp <?= $value["price"] ;?></b>
				<hr>
				<p>
					<input type="radio" name="female_dress_id" value="<?=$value["id"].'|'.$value['price'];?>" required> Pilih
				</p>
			</div>

			<?php
		}
			?>

			<hr>
			<div class="text-end">
				<button class="btn btn-primary">Checkout</button>
			</div>
			</div>
		</form>
	</div>
</div>

</body>
</html>